//
//  EdgeDetcet.h
/**
对EdgeCompute类的定义
Created by 江 裕诚 on 13-4-26.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/

#ifndef BitmapDetect_EdgeDetect_h
#define BitmapDetect_EdgeDetect_h

#include "bmp.h"
#include <string>

namespace bmp{


class EdgeDetect
{
public:
    virtual BYTE** detectArithmetic(BYTE** data, LONG width, LONG height) = 0;
};

class Threshold : public EdgeDetect
{
public:
    BYTE** detectArithmetic(BYTE** data, LONG width, LONG height);
};


class Roberts : public EdgeDetect
{
public:
    BYTE** detectArithmetic(BYTE** data, LONG width, LONG height);
};


class Prewitt : public EdgeDetect
{
public:
    BYTE** detectArithmetic(BYTE** data, LONG width, LONG height);
};

class Sobel : public EdgeDetect
{
public:
    BYTE** detectArithmetic(BYTE** data, LONG width, LONG height);
};

}
bmp::EdgeDetect* getDetect(std::string arithmetic);

#endif
