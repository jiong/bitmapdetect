
#ifndef BitmapDetect_Bmp_h
#define BitmapDetect_Bmp_h

#include "def.h"

typedef struct tagBITMAPFILEHEADER
{
    WORD bfType;
    /* 文件类型， 字符串表示位"BM"，十进制
     表示为19778，十六进制表示4D42H */
    DWORD bfSize;//以字节表示的文件长
    WORD bfReserved1;
    WORD bfReserved2;//保留字，须为0
    DWORD bfOffBits;
    //位图数据区距头文件的偏移量
}BITMAPFILEHEADER; //共14字节

typedef struct tagBITMAPINFOHEADER
{
    DWORD biSize; /*本结构长度40 */
    LONG biWidth; /*以象素为单位的位图大小*/
    LONG biHeight;
    WORD biPlanes; /*位平面数，为1*/
    WORD biBitCount;/*每一象素的位数 */
    DWORD biCompression; /*是否压缩*/
    DWORD biSizeImage;/*图像字节大小*/
    LONG biXPelsPerMeter; /*分辨率*/
    LONG biYPelsPerMeter;
    DWORD biClrUsed; /*颜色表中实际使用和最重要的颜色数 */
    DWORD biClrImportant;
}BITMAPINFOHEADER;


#endif // BMP_H_INCLUDED
