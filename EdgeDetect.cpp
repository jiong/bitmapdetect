//
// EdgeCompute.cpp
/**
对EdgeCompute类的定义的实现
Created by 江 裕诚 on 13-5-3.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/


#include "EdgeDetcet.h"
#include <cmath>
#include <iostream>
#include <cstdlib>


BYTE** bmp::Threshold::detectArithmetic(BYTE** data, LONG width, LONG height)
{
	if (data==NULL || data == (BYTE**)0xcccccccc)
	{
		std::cerr<<"请先进行灰度处理，再进行边缘提取！";
		exit(1);
	}

    int grayValue[256] = {0};
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            int tmp = data[i][j];
            grayValue[tmp]++;
        }
    }
    int threshold = 0;
    for (int i = 0; i < 256; i++)
    {
        threshold += (i*grayValue[i]);
    }
    threshold /= (width*height);
    //cout<<"threshold:"<<threshold<<endl;
    //将图片二值化
    BYTE** binData = new BYTE*[width];
    for (int i = 0; i < width; i++)
    {
        binData[i] = new BYTE[height];
    }
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            if (data[i][j] >= threshold)
            {
                binData[i][j] = 0;
            }
            else
            {
                binData[i][j] = 255;
            }
        }
    }
    return binData;
}

BYTE** bmp::Roberts::detectArithmetic(BYTE** data, LONG width, LONG height)
{
	if (data==NULL || data == (BYTE**)0xcccccccc)
	{
		std::cerr<<"请先进行灰度处理，再进行边缘提取！";
		exit(1);
	}

    BYTE** newData;
    newData = new BYTE*[width];
    for (int i = 0; i < width; i++)
    {
        newData[i] = new BYTE[height];
    }
	BYTE pixel[4];

	for (int j = height -1; j > 0; j--)
	{
		for (int i = 0; i < width -1; i++)
		{
			pixel[0] = data[i][j];
			pixel[1] = data[i+1][j];
			pixel[2] = data[i][j-1];
			pixel[3] = data[i+1][j-1];

			newData[i][j] = sqrt((pixel[0] - pixel[3])*(pixel[0] - pixel[3]) +
				(pixel[1] - pixel[2])*(pixel[1] - pixel[2]));

		}
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (newData[i][j] == 0)
			{
				newData[i][j] = 255;
			}
		}
	}

    return newData;
}

BYTE** bmp::Prewitt::detectArithmetic(BYTE** data, LONG width, LONG height)
{
	if (data==NULL || data == (BYTE**)0xcccccccc)
	{
		std::cerr<<"请先进行灰度处理，再进行边缘提取！";
		exit(1);
	}

    BYTE** newData;
	newData = new BYTE*[width];
	for (int i = 0; i < width; i++)
	{
		newData[i] = new BYTE[height];
	}
	double tmp1;
	double tmp2;

	BYTE pixel[8] = {255,255,255,255,255,255,255,255};
	for (int j = height - 1; j > 0; j--)
	{
		for (int i = 1; i < width - 1; i++)
		{

			pixel[0] = data[i-1][j-1];
			pixel[1] = data[i-1][j];
			pixel[2] = data[i-1][j+1];

			pixel[3] = data[i][j-1];
			pixel[4] = data[i][j+1];

			pixel[5] = data[i+1][j-1];
			pixel[6] = data[i+1][j];
			pixel[7] = data[i+1][j+1];
			tmp1 = std::abs(pixel[5]+pixel[6]+pixel[7]-pixel[0]-pixel[1]-pixel[2]);
			tmp2 = std::abs(pixel[2]+pixel[4]+pixel[7]-pixel[0]-pixel[1]-pixel[2]);
			newData[i][j] = (tmp1>tmp2?tmp1:tmp2);
		}
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (newData[i][j] == 0)
			{
				newData[i][j] = 255;
			}
		}
	}
    return newData;

}

BYTE** bmp::Sobel::detectArithmetic(BYTE** data, LONG width, LONG height)
{
	if (data==NULL || data == (BYTE**)0xcccccccc)
	{
		std::cerr<<"请先进行灰度处理，再进行边缘提取！";
		exit(1);
	}

	BYTE** newData;
	newData = new BYTE*[width];
	for (int i = 0; i < width; i++)
	{
		newData[i] = new BYTE[height];
	}
	double tmp1;
	double tmp2;

	BYTE pixel[8] = {255,255,255,255,255,255,255,255};
	for (int j = height - 1; j > 0; j--)
	{
		for (int i = 1; i < width - 1; i++)
		{

			pixel[0] = data[i-1][j-1];
			pixel[1] = data[i-1][j];
			pixel[2] = data[i-1][j+1];

			pixel[3] = data[i][j-1];
			pixel[4] = data[i][j+1];

			pixel[5] = data[i+1][j-1];
			pixel[6] = data[i+1][j];
			pixel[7] = data[i+1][j+1];
			tmp1 = std::abs(3*pixel[2]+10*pixel[4]+3*pixel[7]-3*pixel[0]-10*pixel[3]-3*pixel[4]);
			tmp2 = std::abs(3*pixel[0]+10*pixel[1]+3*pixel[2]-3*pixel[5]-10*pixel[6]-3*pixel[7]);
			//newData[i][j] = tmp1 - tmp2;
			newData[i][j] = (tmp1>tmp2?tmp1:tmp2);
		}
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (newData[i][j] == 0)
			{
				newData[i][j] = 255;
			}
		}
	}
	return newData;
}



bmp::EdgeDetect* getDetect(std::string arithmetic)
{
    bmp::EdgeDetect* detect = NULL;
    if (arithmetic == "Sobel")
    {
        detect = new bmp::Sobel();
    }
    if (arithmetic == "Prewitt")
    {
        detect = new bmp::Prewitt();
    }
    if (arithmetic == "Roberts" )
    {
        detect = new bmp::Roberts();
    }
    if (arithmetic == "Threshold")
    {
        detect = new bmp::Threshold();
    }
    return detect;
}
