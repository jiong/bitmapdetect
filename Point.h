//
//  Point.h
/**
对Point类的定义
Created by 江 裕诚 on 13-5-3.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/
#ifndef BitmapDetect_Point_h
#define BitmapDetect_Point_h

#include "bmp.h"
#include <iostream>

namespace bmp{

class Point
{
private:
    UINT x;
    UINT y;
    bool visited;

public:
    Point(void);
    Point(UINT x, UINT y);
    Point(const Point& p);
    ~Point(void);

    void setX(UINT x);
    void setY(UINT y);
    void setVisited();
    void setVisited(bool);
    bool isVisited();
    UINT getX();
    UINT getY();
    Point* up();
    Point* down();
    Point* right();
    Point* left();
    Point* upAndRight();
    Point* upAndLeft();
    Point* downAndRight();
    Point* downAndLeft();

    bool operator == (const Point& point);

    //friend std::ostream& operator << (std::ostream& out, bmp::Point& point);
};

}

std::ostream& operator << (std::ostream& out, bmp::Point& point);

#endif
