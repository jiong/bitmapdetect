
#include "Bitmap.h"
#include "Point.h"
#include "Notice.h"
#include <cstdlib>
#include <iostream>
#include <cstring>



int main(int argc, const char * argv[])
{

	switch (argc)
	{
	case 3:
		{
			int len = strlen(argv[1]);
			if (argv[1][0]!='-')
			{
				notice();
			}
			else
			{
				if (argv[1][1] == 'v')
				{
					version();
					exit(0);
				}
				char *path = new char[255];
				strcpy(path, argv[2]);
				bmp::Bitmap bmp(path);
				bmp.print();
				for (int i = 1; i<= len; i++)
				{
					if (argv[1][i] == 'e')
					{
						bmp.makeReverse();
					}
					if (argv[1][i] == 'g')
					{
						bmp.makeGray();
					}
					if (argv[1][i] == 'p')
					{
						bmp.makeBin("Prewitt");
					}
					if (argv[1][i] == 's')
					{
						bmp.makeBin("Sobel");
					}
					if (argv[1][i] == 'r')
					{
						bmp.makeBin("Roberts");
					}
					if (argv[1][i] == 't')
					{
						bmp.makeBin("Threshold");
					}
					if (argv[1][i] == 'c')
					{
						bmp.edgeCompute();
					}
					if (argv[1][i] == 'f')
					{
						bmp.createFile();
					}
				}
			}
		}
		break;
	case 2:
		{
			if (strlen(argv[1]) != 2)
			{
				notice();
			}
			else
			{
				if (argv[1][0]!='-')
				{
					notice();
				}else
				{
					if (argv[1][1] == 'v')
					{
						version();
					}
				}
			}

		}
		break;
	default:
		notice();
		break;
	}

    return 0;
}
