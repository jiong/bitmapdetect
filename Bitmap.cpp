//
//  Bitmap.cpp
/**
对Bitmap类的定义的实现
Created by 江 裕诚 on 13-4-23.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/
#include "EdgeCompute.h"

#include "EdgeDetcet.h"
#include "def.h"
#include "Bitmap.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <sstream>
#include <cstring>


/************************************************************************/
/*构造函数，通过文件名构造Bitmap对象                                    */
/*参数：path  文件名或路径                                              */
/************************************************************************/
bmp::Bitmap::Bitmap(char* path)
{

    FILE *fp = fopen(path, "rb");
    if (fp == 0) {
        std::cerr<<"该文件不存在\n";
        exit(1);
    }

	int dir = -1;
	int dot = -1;
	for (int i = 0; i < strlen(path) ; i++)
	{
		if (path[i] == '/' || path[i] == '\\')
		{
			dir = i;
		}
	}
	for (int i = 0; i < strlen(path); i++)
	{
		if (path[i] == '.')
		{
			dot = i;
		}
	}
	std::string typeName;
	for (int i = dot+1; i < strlen(path); i++)
	{
		typeName  += path[i];
	}
	if (typeName != "bmp")
	{
		std::cerr<<"对不起，目前只能处理位图文件！！";
		exit(1);
	}
	for (int i = 0; i <= dir; i++)
	{
		filePath += path[i];
	}
	for (int i = dir+1; i < dot ; i++)
	{
		bmpName += path[i];
	}

    //读取BITMAPFILEHEADER
    BITMAPFILEHEADER bmfh;
    fread(&bmfh, sizeof(BITMAPFILEHEADER), 1, fp);

    size = bmfh.bfSize;
    //读取BITMAPINFOHEADER
    BITMAPINFOHEADER bmih;
    fread(&bmih, sizeof(BITMAPINFOHEADER), 1, fp);
    width = bmih.biWidth;
    height = bmih.biHeight;
    bitCount = bmih.biBitCount;

	if (bitCount != 24)
	{
		std::cerr<<"目前只能处理24位位图!"<<std::endl;
		exit(1);
	}



    //计算每行数据量大小
    linebyte = (width * bitCount / 8 + 3) / 4 * 4;
    bmpData = new BYTE[linebyte * height];
    fread(bmpData, 1, linebyte*height, fp);
    //生成位图数据数组
    makeColorData();
	grayData = NULL;
	binData = NULL;
	reverseData = NULL;
    fclose(fp);
}

bmp::Bitmap::Bitmap(BYTE* bmpDate, LONG width, LONG height)
{
    this->bmpName = width + "_" + height;
    this->bitCount = 24;
    this->bmpData = bmpDate;
    this->width = width;
    this->height = height;
    this->linebyte = (this->width * this->bitCount / 8 + 3) / 4 * 4;
    this->makeColorData();
}

bmp::Bitmap::Bitmap(std::vector<Point> points, LONG width, LONG height, int name)
{

    std::ostringstream swidth;
    swidth<<width;
    std::ostringstream sheight;
    sheight<<height;
    std::ostringstream sname;
    sname<<name;
    this->bmpName = swidth.str() + "_" + sheight.str() + "_" + sname.str();
    this->bitCount = 24;
    this->width = width;
    this->height = height;
    this->linebyte = (this->width * this->bitCount / 8 + 3) / 4 * 4;
    BYTE** data = new BYTE*[this->width];
    for (int i = 0; i < this->width; i++)
    {
        data[i] = new BYTE[height];
    }
    for (int i = 0; i < this->width; i++)
    {
        for (int j = 0; j < this->height; j++)
        {
            data[i][j] = 0;
        }
    }
    std::vector<Point>::iterator itr;
    for (itr = points.begin(); itr != points.end(); itr++)
    {
        Point p = *itr;
        data[p.getX()][p.getY()] = 255;
    }
    this->bmpData = new BYTE[this->linebyte*this->height];
    int k = 0;
    int blank = this->linebyte - (this->width * 3);
    for (int i = 0; i < this->height; i++)
    {
        for (int j = 0; j < this->width; j++)
        {
            this->bmpData[k++] = data[j][i];
            this->bmpData[k++] = data[j][i];
            this->bmpData[k++] = data[j][i];
        }
        for (int n = 0; n < blank; n++)
        {
            this->bmpData[k++] = 0;
        }
    }

    this->makeColorData();
}

//将位图数据转换成位图数据数组
void bmp::Bitmap::makeColorData()
{
    colorData = new BYTE**[width];
    for (int i = 0; i < width; i++)
    {
        colorData[i] = new BYTE*[height];
        for (int j = 0; j < height; j++)
        {
            colorData[i][j] = new BYTE[3];
        }
    }
    int blank = linebyte - (width * 3);
    int k = 0;
    int m = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            colorData[j][i][0] = bmpData[k++];
            colorData[j][i][1] = bmpData[k++];
            colorData[j][i][2] = bmpData[k++];
            //cout<<"["<<(int)dataColor[i][j][0]<<" "<<(int)dataColor[i][j][1]<<" "<<(int)dataColor[i][j][2]<<"]";
            //逢 k == (linebyte * (m+1) - blank) 为间隔符号
            if (k == (linebyte * (m+1) - blank))
            {
                for (int n = 0; n < blank; n++)
                {
                    k++;
                }
                m++;
            }
        }
    }
}

/************************************************************************/
/*makeGray函数，通过每一点的rgb值计算出灰度值                           */
/************************************************************************/
void bmp::Bitmap::makeGrayData()
{
    //1) Gray = (R*299 + G*587 + B*114 + 500) / 1000
    //2) Gray = (R*313524 + G*615514 + B*119538) >> 20
    grayData = new BYTE*[width];
    for (int i = 0; i < width; i++)
    {
        grayData[i] = new BYTE[height];
    }

    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            //1)
            //grayData[i][j] = (dataColor[i][j][0]*114 + dataColor[i][j][1]*587 + dataColor[i][j][2]*299 + 500) / 1000;
            //2)
            grayData[i][j] = (colorData[i][j][0]*119538 + colorData[i][j][1]*615514 + colorData[i][j][2]*313524) >> 20;
        }
    }
}

void bmp::Bitmap::makeGray()
{
    makeGrayData();

    createNewBitmap("gray", grayData);
}

/************************************************************************/
/*print函数，输出bmp文件的信息                              */
/************************************************************************/
void bmp::Bitmap::print()
{
    std::cout<<"文件名 : "<<bmpName<<std::endl;
    std::cout<<"大小 : "<<size<<"字节"<<std::endl;
    std::cout<<"图像宽 : "<<width<<std::endl;
    std::cout<<"图像高 : "<<height<<std::endl;
    //std::cout<<"bitCount = "<<bitCount<<std::endl;
    //std::cout<<"linebyte = "<<linebyte<<std::endl;
}
/************************************************************************/
/* 生成彩色图片                                           */
/*    type 用于对图像的重命名 data 是三维数组               */
/************************************************************************/
void bmp::Bitmap::createNewBitmap(std::string type, BYTE*** data)
{
    std::string newBmpName = filePath + bmpName + "_" + type + "_new.bmp";
    FILE* fp = fopen(newBmpName.c_str(), "wb");

    BITMAPFILEHEADER bmfh;
    bmfh.bfType = 0x4D42;
    bmfh.bfSize = 54 + linebyte * height;
    bmfh.bfReserved1 = 0;
    bmfh.bfReserved2 = 0;
    bmfh.bfOffBits = 54;
    fwrite(&bmfh, sizeof(BITMAPFILEHEADER), 1, fp);

    BITMAPINFOHEADER bmih;
    bmih.biSize = 40;
    bmih.biHeight = height;
    bmih.biWidth = width;
    bmih.biPlanes = 1;
    bmih.biBitCount = bitCount;
    bmih.biCompression = 0;
    bmih.biSizeImage = linebyte * height;
    bmih.biXPelsPerMeter = 0xB12;
    bmih.biYPelsPerMeter = 0xB12;
    bmih.biClrUsed = 0;
    bmih.biClrImportant = 0;
    fwrite(&bmih, sizeof(BITMAPINFOHEADER), 1, fp);

    char space = (char)'0x00';
    int blank = linebyte - (width * 3);
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            fwrite(&data[j][i][0], 1, 1, fp);
            fwrite(&data[j][i][1], 1, 1, fp);
            fwrite(&data[j][i][2], 1, 1, fp);
        }
        for (int k = 0; k < blank; k++)
        {
            fwrite(&space, 1, 1, fp);
        }
    }

    fclose(fp);
}

void bmp::Bitmap::createFile()
{
    std::string name = bmpName;
    name = name + "_file" + ".txt";
    FILE* fp = fopen(name.c_str(), "w+");
    for (int i = 0; i < height; i++)
    {
        fprintf(fp, "//line %d\n", i);
        for (int j = 0; j< width; j++)
        {
            fprintf(fp, "[%lx,%lx,%lx] ", colorData[j][i][0],colorData[j][i][1],colorData[j][i][2]);
        }
        fprintf(fp, "\n");
    }

    fclose(fp);
}

void bmp::Bitmap::makeBinData(std::string sdetect, BYTE** data)
{
	if (binData == NULL || binData == (BYTE**)0xcccccccc)
	{
		binData = new BYTE*[width];
		for (int i = 0; i < width; i++)
		{
			binData[i] = new BYTE[height];
		}
	}
    //std::string sdetect = "Sobel";
    //std::string sdetect = "Prewitt";
    //std::string sdetect = "Roberts";
    //std::string sdetect = "Threshold";
    bmp::EdgeDetect* detect = getDetect(sdetect);

    binData = detect->detectArithmetic(data, width, height);
}

void bmp::Bitmap::makeBin(std::string sdetect)
{
	if (sdetect == "Threshold")
	{
		if (binData == NULL || binData == (BYTE**)0x0000000f)
		{
			makeBinData(sdetect,grayData);
			//生成图片
			createNewBitmap(sdetect, binData);
		}
	}
	else
	{
		makeBinData(sdetect, grayData);
		makeBinData("Threshold", binData);
		//生成图片
		createNewBitmap(sdetect, binData);
	}



}

/************************************************************************/
/* 生成单色图片                                           */
/*    type 用于对图像的重命名 data 是二维数组               */
/************************************************************************/
void bmp::Bitmap::createNewBitmap(std::string type, BYTE** data)
{
    std::string newBmpName = filePath + bmpName + "_" + type + "_new.bmp";
    FILE* fp = fopen(newBmpName.c_str(), "wb");

    BITMAPFILEHEADER bmfh;
    bmfh.bfType = 0x4D42;
    bmfh.bfSize = 54 + linebyte * height;
    bmfh.bfReserved1 = 0;
    bmfh.bfReserved2 = 0;
    bmfh.bfOffBits = 54;
    fwrite(&bmfh, sizeof(BITMAPFILEHEADER), 1, fp);

    BITMAPINFOHEADER bmih;
    bmih.biSize = 40;
    bmih.biHeight = height;
    bmih.biWidth = width;
    bmih.biPlanes = 1;
    bmih.biBitCount = bitCount;
    bmih.biCompression = 0;
    bmih.biSizeImage = linebyte * height;
    bmih.biXPelsPerMeter = 0xB12;
    bmih.biYPelsPerMeter = 0xB12;
    bmih.biClrUsed = 0;
    bmih.biClrImportant = 0;
    fwrite(&bmih, sizeof(BITMAPINFOHEADER), 1, fp);

    char space = (char)'0x00';
    int blank = linebyte - (width * 3);
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            fwrite(&data[j][i], 1, 1, fp);
            fwrite(&data[j][i], 1, 1, fp);
            fwrite(&data[j][i], 1, 1, fp);
        }
        for (int k = 0; k < blank; k++)
        {
            fwrite(&space, 1, 1, fp);
        }
    }

    fclose(fp);
}
/************************************************************************/
/* 生成负相图片                                           */
/************************************************************************/
void bmp::Bitmap::makeReverseData()
{
    reverseData = new BYTE**[width];
    for (int i = 0; i < width; i++)
    {
        reverseData[i] = new BYTE*[height];
        for (int j = 0; j < height ; j++)
        {
            reverseData[i][j] = new BYTE[3];
        }
    }

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                reverseData[j][i][k] = 255 - colorData[j][i][k];
            }
        }
    }
}

void bmp::Bitmap::makeReverse()
{
    makeReverseData();

    createNewBitmap("reverse", reverseData);
}

void bmp::Bitmap::createNewBitmap()
{
    createNewBitmap("", colorData);
}

void bmp::Bitmap::edgeCompute()
{
    EdgeCompute ec(binData, width, height);
    int count = ec.getCount();
    std::cout<<"图片可分割成"<<count<<"块"<<std::endl;
    //ec.print();
    //cout<<"edgeCompute";
}
