//Notice.cpp
//
/**
实现输出函数
Created by 江 裕诚 on 13-4-23.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/



#include "Notice.h"

void notice()
{
    std::cout<<"这是一个关于数字图像处理的小工具。\n";
    std::cout<<"用法： BitmapDetect [选项].. [文件]\n";
    std::cout<<"此工具可以实现将位图文件进行一些简单的处理\n";
    std::cout<<"选项中可以使用的命令如下：\n";
    std::cout<<"\t-g\t\t将图片变成灰度图片。\n";
    std::cout<<"\t-p\t\t将图片进行Prewitt算子检测。\n";
    std::cout<<"\t-s\t\t将图片进行Sobel算子检测。\n";
    std::cout<<"\t-r\t\t将图片进行Robert算子检测。\n";
    std::cout<<"\t-t\t\t将图片进行阈值二值化处理算子检测。\n";
    std::cout<<"\t-c\t\t将图片进行分割，并输出图块数。\n";
    std::cout<<"\t-f\t\t生成图片像素文件\n";
    std::cout<<"!!!!特别说明:\n";
    std::cout<<"选项可以连用，如想让图片灰度后进行Robert算子检测，并进行图像分割。可直接输入： \n";
    std::cout<<"\t-grc\n";
    std::cout<<"所有算子检测，二值化处理即图像分割，都必须跟在-g之后\n";
    std::cout<<"\t-e\t\t将图片变成负相图片。\n";

    std::cout<<"\t-v\t\t输出软件版本。\n";

    std::cout<<"退出状态：\n";
    std::cout<<" 0\t正常退出，\n";
    std::cout<<" 1\t异常退出，\n";
}

void version()
{
    std::cout<<"+----------------------------------------------------------+\n";
    std::cout<<"|   BitmapDetect v1.0                                      |\n";
    std::cout<<"|   作者：江裕诚                                           |\n";
    std::cout<<"|   本软件遵循GPL 2.1协议，使用前请自行阅读GPL协议的内容。 |\n";
    std::cout<<"+----------------------------------------------------------+\n";
}
