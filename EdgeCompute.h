//
//  EdgeCompute.h
//  BMPTest
/**
对EdgeCompute类的定义
Created by 江 裕诚 on 13-5-3.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/
#ifndef BitmapDetect_EdgeCompute_h
#define BitmapDetect_EdgeCompute_h

#include "bmp.h"
#include "Point.h"
#include <vector>
#include <algorithm>
#include <list>
#include "Bitmap.h"


namespace bmp{

class EdgeCompute
{
    std::vector<bmp::Point> points;
    //新建一个集合
    std::vector< std::vector<bmp::Point> > tagPoints;
    LONG width;
    LONG height;
    LONG length;
    int count;
    void execute(bmp::Point*, std::vector<bmp::Point>&);
    void compute();
    void setCount();
    bmp::Point* getNeighbor(bmp::Point&);
    void denoise();
public:
    EdgeCompute(BYTE** data, LONG width, LONG height);
    ~EdgeCompute(void);
    int getCount();
    void print();
};

}

#endif
