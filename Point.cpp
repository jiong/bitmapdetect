//
//  Point.cpp
/**
对Point类的定义
Created by 江 裕诚 on 13-5-3.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.  


*/

#include "Point.h"

bmp::Point::Point(void)
{
    x = 0;
    y = 0;
    visited = false;
}

bmp::Point::Point(UINT x, UINT y)
{
    this->x = x;
    this->y = y;
    this->visited = false;
}

bmp::Point::Point(const bmp::Point& p)
{
    this->x = p.x;
    this->y = p.y;
    this->visited = p.visited;
}

bmp::Point::~Point(void)
{
}

UINT bmp::Point::getX()
{
    return this->x;
}

UINT bmp::Point::getY()
{
    return this->y;
}

void bmp::Point::setVisited()
{
    this->visited = true;
}

void bmp::Point::setVisited(bool visited)
{
    this->visited = visited;
}

bool bmp::Point::isVisited()
{
    return visited;
}

void bmp::Point::setX(UINT x)
{
    this->x = x;
}

void bmp::Point::setY(UINT y)
{
    this->y = y;
}

bmp::Point* bmp::Point::up()
{   
    Point* p = new Point(this->getX(), (this->getY())+1);
    return p;
}

bmp::Point* bmp::Point::down()
{
    if (this->y >= 0)
    {
        Point* p = new Point(this->getX(), (this->getY())-1);
        return p;
    }
    return NULL;
}

bmp::Point* bmp::Point::right()
{
    Point* p = new Point((this->getX())+1, this->getY());
    return p;
}

bmp::Point* bmp::Point::left()
{
    if (this->x >= 0)
    {
        Point* p = new Point((this->getX())-1, this->getY());
        return p;
    }
    return NULL;
}

bmp::Point* bmp::Point::upAndRight()
{
    Point* p = new Point((this->getX())+1, (this->getY())+1);
    return p;
}

bmp::Point* bmp::Point::upAndLeft()
{
    if (this->x >= 0)
    {
        Point* p = new Point((this->getX())-1, (this->getY())+1);
        return p;
    }
    return NULL;
}


bmp::Point* bmp::Point::downAndRight()
{
    if (this->y >= 0)
    {
        Point* p = new Point((this->getX())+1, (this->getY())-1);
        return p;
    }
    return NULL;
}

bmp::Point* bmp::Point::downAndLeft()
{
    if (this->y >= 0 && this->x >= 0)
    {
        Point* p = new Point((this->getX())-1, this->getY());
        return p;
    }
    return NULL;
}

bool bmp::Point::operator == (const bmp::Point& point)
{
    if (this->x == point.x && this->y == point.y)
    {
        return true;
    } 
    else
    {
        return false;
    }
}

std::ostream& operator << (std::ostream& out, bmp::Point& point)
{
    out<<"x="<<point.getX()<<" y="<<point.getY()<<"\n";
    return out;
}