//
//  Bitmap.h
/**
对Bitmap类的定义
Created by 江 裕诚 on 13-4-23.

This file is part of the Bitmap Detcet tools.

The Bitmap Detcet tools is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

The Bitmap Detcet tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with the Bitmap Detcet tools; if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA.


*/
#ifndef BitmapDetect_Bitmap_h
#define BitmapDetect_Bitmap_h

#include "bmp.h"
#include <string>
#include <vector>
#include "def.h"
#include "Point.h"

namespace bmp{


class Bitmap {
private:
    std::string bmpName;
    LONG width;
    LONG height;
    WORD bitCount;
    DWORD size;
    BYTE* bmpData;//位图数据
    int linebyte;//每行位图数据的长度
    BYTE*** colorData;//储存位图数据为bgr数组
    BYTE** grayData;//灰度数组
    BYTE** binData;//二值图像数据
    BYTE*** reverseData;//负相图像数据

    void makeColorData();
    void makeGrayData();
    void makeReverseData();
    void makeBinData(std::string, BYTE**);
public:
    Bitmap(char* path);
    Bitmap(BYTE* bmpDate, LONG width, LONG height);
    Bitmap(std::vector<Point>, LONG width, LONG height, int name);
    void makeGray();//将dataColor中的数据，进行灰度处理，存入grayData中
    void print();
    void createNewBitmap(std::string type, BYTE*** data);
    void createNewBitmap(std::string type, BYTE** data);
    void createNewBitmap();
    void makeBin(std::string);
    void createFile();
    void makeReverse();
    void edgeCompute();
};

}
#endif
